<p align="center">
  <img src="https://img.icons8.com/?size=512&id=55494&format=png" width="100" />
</p>
<p align="center">
    <h1 align="center">FAMS-BACKEND</h1>
</p>

<p align="center">
	<img src="https://img.shields.io/badge/YAML-CB171E.svg?style=for-the-badge&logo=YAML&logoColor=white" alt="YAML">
	<img src="https://img.shields.io/badge/Docker-2496ED.svg?style=for-the-badge&logo=Docker&logoColor=white" alt="Docker">
	<img src="https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=openjdk&logoColor=white" alt="java">
	<img src="https://img.shields.io/badge/Spring-000000.svg?style=for-the-badge&logo=Spring&logoColor=white" alt="Spring">
</p>

## Dockerized Project 🚀🚀
1. Install Docker on Window [click here](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe?_gl=1*3jwbnk*_ga*MTU2MzcwNDM2OS4xNjk3NDU2NjY2*_ga_XJWPQMJYHQ*MTY5NzUyNjQ5Mi44LjEuMTY5NzUyODUxMy40My4wLjA.)
2. Check Docker version after install 
> ```bash
> docker --version
> Docker <version>
>```

3. Clone project then build
>```bash
>git clone https://gitlab.com/Nirancode4f/fams-backend.git
>cd FFood-shop
>## Run docker
>docker compose up --build
>```

Now, you can check swagger locally on your machine at: 
```
http://localhost:8080/swagger-ui/index.html
```

Postman: https://documenter.getpostman.com/view/18903984/2s9YysENFo


---
